from setuptools import setup, find_packages


setup(
    name='kukulkan-standalone',
    version='0.1.0',
    description='Kukulkan stand-alone application.',
    author='',
    author_email='',
    license='MIT',
    py_modules=['kukulkan_standalone'],
    entry_points={
        'kukulkan.apps': ['Kukulkan=kukulkan_standalone:main']
    },
    url='https://gitlab.com/kukulkan/apps/kukulkan-standalone',
    download_url='git+https://gitlab.com/kukulkan/apps/kukulkan-standalone.git#egg=kukulkan-standalone',
    install_requires=[
        'kukulkan',
        'value-nodes',
        'math-nodes',
        'attribute-types',
    ],
    dependency_links=[
        'git+https://gitlab.com/kukulkan/kukulkan#egg=kukulkan',
        'git+https://gitlab.com/kukulkan/core-packages/value-nodes#egg=value-nodes',
        'git+https://gitlab.com/kukulkan/core-packages/math-nodes#egg=math-nodes',
        'git+https://gitlab.com/kukulkan/core-packages/attribute-types#egg=attribute-types',
    ]
)
